class Trie(object):
    def __init__(self):
        self.nextLetters = {}

    def insert(self, word):
        """
        :type word: str
        :rtype: None
        """
        if word:
            if not (word[0] in self.nextLetters.keys()):
                self.nextLetters[word[0]] = Trie()
            self.nextLetters[word[0]].insert(word[1:])
        else:
            self.nextLetters["end"] = None

    def search(self, word):
        """
        :type word: str
        :rtype: bool
        """
        if not word:
            return "end" in self.nextLetters.keys()
        else:
            if(word[0] in self.nextLetters.keys()):
                return self.nextLetters[word[0]].search(word[1:])
            else:
                return False

    def startsWith(self, prefix):
        """
        :type prefix: str
        :rtype: bool
        """
        if not prefix:
            return True
        else:
            if(prefix[0] in self.nextLetters.keys()):
                return self.nextLetters[prefix[0]].startsWith(prefix[1:])
            else:
                return False
        


# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)